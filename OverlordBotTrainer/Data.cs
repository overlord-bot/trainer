﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace OverlordBotTrainer
{
    class Data
    {
        public static readonly Dictionary<string, List<string>> CaucasusAirfieldNames = new Dictionary<string, List<string>>
        {
            { "Anapa-Vityazevo", new List<string> { "Anapa-Vityazevo", "Anapa" } },
            { "Beslan", new List<string> { "Beslan" } },
            { "Gelendzhik", new List<string> { "Gelendzhik", "gelend", "gelen" } },
            { "Gudauta", new List<string> { "Gudauta" } },
            { "Kobuleti", new List<string> { "Kobuleti" } },
            { "Krasnodar-Center", new List<string> { "Krasnodar-Center", "Kras Center" } },
            { "Krasnodar-Pashkovsky", new List<string> { "Krasnodar-Pashkovsky", "Kras Pash", "Pashkovsky" } },
            { "Krymsk", new List<string> { "Krymsk" } },
            { "Kutaisi", new List<string> { "Kutaisi" } },
            { "Maykop-Khanskaya", new List<string> { "Maykop-Khanskaya", "Maykop", "Khanskaya" } },
            { "Mineralnye Vody", new List<string> { "Mineralnye Vody", "Mineralnye", "Vody", "Minvody" } },
            { "Mozdok", new List<string> { "Mozdok" } },
            { "Nalchik", new List<string> { "Nalchik" } },
            { "Novorossiysk", new List<string> { "Novorossiysk", "novo", "novoro" } },
            { "Senaki-Kolkhi", new List<string> { "Senaki-Kolkhi", "Senaki", "Kolkhi" } },
            { "Sochi-Adler", new List<string> { "Sochi-Adler", "Sochi" } },
            { "Soganlug", new List<string> { "Soganlug" } },
            { "Sukhumi-Babushara", new List<string> { "Sukhumi-Babushara", "Sukhumi", "Babushara" } },
            { "Tbilisi-Lochini", new List<string> { "Tbilisi-Lochini", "Tbilisi" } },
            { "Vaziani", new List<string> { "Vaziani", "Vaz" } },
        };

        public static readonly Dictionary<string, List<string>> PersianGulfAirfieldNames = new Dictionary<string, List<string>>
        {
            { "Abu Musa", new List<string> { "Abu Musa" } },
            { "Bandar Abbas", new List<string> { "Bandar Abbas", "Abbas" } },
            { "Bandar Lengeh", new List<string> { "Bandar Lengeh", "Lengeh" } },
            { "Al Dhafra", new List<string> { "Al Dhafra", "Dhafra" } },
            { "Dubai", new List<string> { "Dubai", "Dubai International" } },
            { "Al Maktoum", new List<string> { "Al Maktoum", "Al Maktoum International", "Maktoum" } },
            { "Fujairah", new List<string> { "Fujairah", "Fujairah International" } },
            { "Tunb Island", new List<string> { "Tunb Island" } },
            { "Havadarya", new List<string> { "Havadarya" } },
            { "Khasab", new List<string> { "Khasab" } },
            { "Lar", new List<string> { "Lar" } },
            { "Al Minhad", new List<string> { "Al Minhad" } },
            { "Qeshm", new List<string> { "Qeshm", "Qeshm Island" } },
            { "Sharjah", new List<string> { "Sharjah", "Sharjah International" } },
            { "Sirri", new List<string> { "Sirri", "Sirri Island" } },
            { "Tunb Kochak", new List<string> { "Tunb Kochak", "Kochak" } },
            { "Sir Abu Nuayr", new List<string> { "Sir Abu Nuayr", "Abu Nuayr" } },
            { "Kerman", new List<string> { "Kerman" } },
            { "Shiraz", new List<string> { "Shiraz", "Shiraz International" } },
            { "Sas Al Nakheel", new List<string> { "Sas Al Nakheel" } },
            { "Bandar-e-Jask", new List<string> { "Bandar-e-Jask" } },
            { "Abu Dhabi", new List<string> { "Abu Dhabi" } },
            { "Al-Bateen", new List<string> { "Al-Bateen" } },
            { "Kish", new List<string> { "Kish", "Kish Island" } },
            { "Al Ain", new List<string> { "Al Ain", " Al Ain International" } },
            { "Lavan", new List<string> { "Lavan", " Lavan Island" } },
            { "Jiroft", new List<string> { "Jiroft" } },
            { "Ras Al Khaimah", new List<string> { "Ras Al Khaimah", " Ras Al" } },
            { "Liwa", new List<string> { "Liwa" } },
        };

        public static readonly Dictionary<string, List<string>> SyriaAirfieldNames = new Dictionary<string, List<string>>
        {
            {"Abu al-Duhur", new List<string> { "Abu al-Duhur", "al-Duhur" } },
            {"Adana Sakirpasa", new List<string> { "Adana Sakirpasa", "Adana", "Sakirpasa" } },
            {"Al Qusayr", new List<string> { "Al Qusayr", "Qusayr" } },
            {"An Nasiriyah", new List<string> { "An Nasiriyah", "Nasiriyah" } },
            {"Beirut-Rafic Hariri", new List<string> { "Beirut-Rafic Hariri", "Beirut", "Beirut-Rafic", "Rafic Hariri", "Rafic", "Hariri" } },
            {"Damascus", new List<string> { "Damascus" } },
            {"Marj as Sultan South", new List<string> { "Marj as Sultan South", "Marj South", "Sultan South" } },
            {"Al-Dumayr", new List<string> { "Al-Dumayr" } },
            {"Eyn Shemer", new List<string> { "Eyn Shemer", "Shemer" } },
            {"Haifa", new List<string> { "Haifa" } },
            {"Hama", new List<string> { "Hama" } },
            {"Hatay", new List<string> { "Hatay" } },
            {"Incirlik", new List<string> { "Incirlik" } },
            {"Jirah", new List<string> { "Jirah" } },
            {"Khalkhalah", new List<string> { "Khalkhalah" } },
            {"King Hussein Air College", new List<string> { "King Hussein Air College", "King Hussein", "Hussein" } },
            {"Kiryat Shmona", new List<string> { "Kiryat Shmona", "Kiryat", "Shmona" } },
            {"Bassel Al-Assad", new List<string> { "Bassel Al-Assad", "Bassel", "Al-Assad" } },
            {"Marj as Sultan North", new List<string> { "Marj as Sultan North", "Marj North", "Sultan North" } },
            {"Marj Ruhayyil", new List<string> { "Marj Ruhayyil", "Marj", "Ruhayyil" } },
            {"Megiddo", new List<string> { "Megiddo" } },
            {"Mezzeh", new List<string> { "Mezzeh" } },
            {"Minakh", new List<string> { "Minakh" } },
            {"Aleppo", new List<string> { "Aleppo" } },
            {"Palmyra", new List<string> { "Palmyra" } },
            {"Qabr as Sitt", new List<string> { "Qabr as Sitt", "Qabr", "Sitt" } },
            {"Ramat David", new List<string> { "Ramat David", "Ramat", "David" } },
            {"Kuweires", new List<string> { "Kuweires" } },
            {"Rayak", new List<string> { "Rayak" } },
            {"Rene Mouawad", new List<string> { "Rene Mouawad", "Rene" } },
            {"Tabqa", new List<string> { "Tabqa" } },
            {"Taftanaz", new List<string> { "Taftanaz" } },
            {"Wujah Al Hajar", new List<string> { "Wujah Al Hajar", "Wujah" } }
        };

        public static readonly Array TaxiAirfieldControllers = new ArrayList { "ground", "tower", "traffic" }.ToArray();
        public static readonly Array TaxiCalls = new ArrayList { "ready to taxi", "permission to taxi", "request permission to taxi" }.ToArray();
        public static readonly Array Runways = new ArrayList { "runway two two", "runway zero four", "runway three one left", "runway one three right" }.ToArray();

        public static readonly Array AwacsCallsigns = new ArrayList { "overlord", "magic", "darkstar", "screwtop", "wizard", "skynet" }.ToArray();
        public static readonly Array RadioCheckCalls = new ArrayList { "radio check", "comm check", "comms check", "radio check on one three six" }.ToArray();
        public static readonly Array BogeyDopeCalls = new ArrayList { "bogey dope", "shopping" }.ToArray();
        public static readonly Array SetWarningRadiusCalls = new ArrayList { "set warning radius", "warn me when bandits get within", "set threat radius", "set perimeter" }.ToArray();
        public static readonly Array SetWarningRadiusMiles = new ArrayList { "ten", "twenty", "forty five", "thirty two", "fifteen", "fifty" }.ToArray();
    }
}
