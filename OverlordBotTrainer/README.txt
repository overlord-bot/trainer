# Introduction

OverlordBotTrainer allows you to make audio recordings that can then be used to train OverlordBot on your voice and accent to improve recognition rates for your calls.

## Instructions

Run `OverlordBotTrainer.exe` to start the process. Follow the instructions in the application to record your sample calls.

Once you are finished upload the created Zip file to the OverlordBot Discord ( https://discord.gg/9RqyTJt ) server in the #voice-recordings channel.

## Problems

If you have any problems or comments please contact `DOLT 1-2 | RurouniJones` on the OverlordBot Discord ( https://discord.gg/9RqyTJt ) server

## Privacy
1. Supplied voice recordings will be solely used to train OverlordBot, are only accessible by the Developer (`DOLT 1-2 | RurouniJones`) and will not be shared.
2. OverlordBot uses Microsoft Cognitive Services for voice recognition and Language understanding, see their privacy policy at https://azure.microsoft.com/en-us/support/legal/cognitive-services-compliance-and-privacy/
3. See Discord's privacy policy at https://discord.com/privacy
